CREATE PROCEDURE replica_identity_full
language plpgsql
as $$
DECLARE
    local_table_names varchar[];
    local_index integer;
BEGIN
    local_table_names = ARRAY(
		select table_name
		from information_schema.tables
    	where table_schema not in ('information_schema', 'pg_catalog'));
    for local_index in array_lower(local_table_names, 1)..array_upper(local_table_names, 1)
	loop
        EXECUTE 'ALTER TABLE ' || quote_ident(local_table_names[local_index]) || ' REPLICA IDENTITY FULL';
        RAISE NOTICE 'Table % REPLICA IDENTITY set to FULL', local_table_names[local_index];
    end loop;
    RAISE NOTICE 'Tables updated: %', array_length(local_table_names, 1);
END;
$$;