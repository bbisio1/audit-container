# audit-container

## Configuring the PostgreSQL server:

- Set wal_level to "logical"
```
ALTER SYSTEM SET wal_level = logical;
```
- Restart server so that the change is persisted. In order to observe the change run the following query
and check that the value of "setting" is "logical".
```
SELECT * FROM pg_settings WHERE name ='wal_level';
```
- The following querys have the purpose of creating a user and give him enough 
privileges so that the debezium connector that will be using the user is able to
access the database and read the wal.
```
CREATE USER top_audit WITH LOGIN REPLICATION ENCRYPTED PASSWORD 'password';
GRANT CONNECT ON DATABASE top to top_audit;
GRANT USAGE ON SCHEMA public to top_audit;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO top_audit;
GRANT ALL PRIVILEGES ON DATABASE top TO top_audit;
GRANT top TO top_audit;
```
- Add entries to the pg_hba.conf file to specify the Debezium connector hosts that can replicate with the database host.
```
ALTER SYSTEM SET listen_addresses TO '*';
```
- Check if it was added.
```
select * from pg_settings where name ='listen_addresses';
```
- Create a publication for all the tables that will be auditable
```
CREATE PUBLICATION "top_publication" FOR ALL TABLES;
```
- Add to pg_hba.conf (change id)
```
host    replication     top_audit   192.168.0.110/32    scram-sha-256
```
- Run the store procedure in ./Scripts/replica_identity_full.sql in order to 
set all tables Replica Identity to Full.

# Connectors Configuration:

## PostgresConnector (example)

### Using Confluent Connect

- Error Handling
  - Retry timeout for errors: 10000
  - Error Tolerance: all
  - Log errors: true
  - Log error details: true

- Postgres
  - Namespace: postgreSQL
  - Hostname: 192.168.0.110
  - Port: 5432
  - User: top_audit
  - Password: password
  - Database: top
  - Plugin: pgoutput
  - Slot: top_audit
  - Publication: top_publication

### Manual request

- URL: http://localhost:9021/api/connect/connect-default/connectors
- Request: POST
- body:
```
{
    "name": "source connector",
    "config": {
        "connector.class": "io.debezium.connector.postgresql.PostgresConnector",
        "database.dbname": "top",
        "database.hostname": "192.168.0.110",
        "database.password": "password",
        "database.port": "5432",
        "database.server.name": "postgreSQL",
        "database.user": "top_audit",
        "database.tcpKeepAlive": true
        "errors.retry.timeout": "10000",
        "errors.tolerance": "all",
        "errors.log.enable": true,
        "errors.log.include.messages": true,
        "plugin.name": "pgoutput",
        "publication.name": "top_publication",
        "slot.name": "top_audit",
    }
}
```

## MongoSinkConnector (example)

### Using Confluent Connect

- Error Tolerance
  - Error Tolerance: all
  - Log errors: true
  - Log error details: true
- Connection
  - MongoDB Connection URI: mongodb://rootuser:rootpass@mongodb:27017
- Namespace
  - The MongoDB database name: top_log
- Change Data Capture
  - The CDC handler: com.mongodb.kafka.connect.sink.cdc.debezium.rdbms.postgres.PostgresHandler

Notes:
  - The view doesn't let the user select all the topics in one go, they have to be selected one by one which is not user-friendly.
  - If more than one topic is selected, once the connector is created it will fail due to "Invalid topics" this happens because the topics string in the json has white spaces which are considered invalid characters.
  - If the user wants to use a topic regular expression in order to group all the topics, first the user needs to select any topic and enter a name, once display the additional options, fill the topic.regex field and delete the topic selected at the beggining.

### Manual request

- URL: http://localhost:9021/api/connect/connect-default/connectors
- Request: POST
- body:

#### Listing all the topics

```
{
  "name": "sink connector",
  "config": {
    "change.data.capture.handler": "com.mongodb.kafka.connect.sink.cdc.debezium.rdbms.postgres.PostgresHandler",
    "collection": "some_collection",
    "connection.uri": "mongodb://rootuser:rootpass@mongodb:27017",
    "connector.class": "com.mongodb.kafka.connect.MongoSinkConnector",
    "database": "top_log_client",
    "errors.log.enable": "true",
    "errors.tolerance": "all",
    "mongo.errors.log.enable": "true",
    "topics": "postgreSQL.public.client,postgreSQL.public.account_validation_config,postgreSQL.public.acc_exchange_rate"
  }
}
```

#### Using a topic regex

```
{
  "name": "sink connector",
  "config": {
    "change.data.capture.handler": "com.mongodb.kafka.connect.sink.cdc.debezium.rdbms.postgres.PostgresHandler",
    "collection": "some_collection",
    "connection.uri": "mongodb://rootuser:rootpass@mongodb:27017",
    "connector.class": "com.mongodb.kafka.connect.MongoSinkConnector",
    "database": "top_log_client",
    "errors.log.enable": "true",
    "errors.tolerance": "all",
    "mongo.errors.log.enable": "true",
    "topics.regex": "postgreSQL\\.public\\..*"
  }
}
```

# More info:
https://debezium.io/documentation/reference/1.7/connectors/postgresql.html
https://docs.mongodb.com/kafka-connector/current/sink-connector/fundamentals/change-data-capture/#change-data-capture-using-debezium
https://kafka.apache.org/documentation/#connect_rest

